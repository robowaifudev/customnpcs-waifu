// http.js
function read(inputStream){
    var inReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
    var inputLine;
    var response = new java.lang.StringBuffer();

    while ((inputLine = inReader.readLine()) != null) {
           response.append(inputLine);
    }
    inReader.close();
    return response.toString();
}

function write(outputStream, data){
    var wr = new java.io.DataOutputStream(outputStream);
    wr.writeBytes(data);
    wr.flush();
    wr.close();
}

function asResponse(con){
    var d = read(con.inputStream);

    return {data : d, statusCode : con.responseCode};
}

function httpGet(theUrl){
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "GET";

    return asResponse(con);
}

function httpPost(theUrl, data, contentType){
    contentType = contentType || "application/json";
    var con = new java.net.URL(theUrl).openConnection();

    con.requestMethod = "POST";
    con.setRequestProperty("Content-Type", contentType);

    // Send post request
    con.doOutput=true;
    write(con.outputStream, data);

    return asResponse(con);
}

function asResponse(con){
    var d = read(con.inputStream);

    return {data : d, statusCode : con.responseCode};
}


// waifu.js
function broadcast(event, name, message) {
  event.npc.world.broadcast(name + ": " + message);
}

var lastChatEvent = null;

function init(event) {
    // listen to the 'playerChat' event
    event.npc.world.broadcast("loaded");
    event.npc.timers.stop(1);
    eventBus().on('playerChat', function(chatEvent, otherArgument){
        //event.npc.world.broadcast('playerChat');
        lastChatEvent = chatEvent;
        event.npc.timers.stop(1);
        event.npc.timers.start(1, 5, true); 
    }, event.npc.UUID); // unique ID to avoid registering same functions!
}

function timer(event) {
  if (event.id == 1) { // id set by timer in init()
    var playerName = lastChatEvent.player.getDisplayName();
    var message = lastChatEvent.message;
    var name = "Waifu";
    var argv = message.split(/\s+/);
    //event.npc.say(message); // debug echo
    event.npc.timers.stop(1); // stop the timer before something breaks
    
    // get response from waifu.py FastAPI
    var response = httpGet("http://localhost:8000/chat?message=" + message.replaceAll(" ", "+"));
    if (response.statusCode != 200) {
        broadcast(event, name, "(Error " + responseStatusCode + ")");
        return;
    }
    var data = JSON.parse(response["data"]);
    broadcast(event, name, data["message"]);
  }
}


// event_bus.js
function eventBus() {
    var API = Java.type('noppes.npcs.api.NpcAPI').Instance();
    var w = API.getIWorld(0);
    var tempdata = w.getTempdata();
    var eventBusName = 'SCRIPT_EVENT_BUS';

    if(!tempdata.has(eventBusName)) {
        var bus = {
            _name: eventBusName,
            _events: {},
            /**
             * Listen to an event
             * @param {String} eventName The event to listen to
             * @param {Function} callback The function to execute
             * @param {String|null} name *OPTIONAL* unique name, for the ability to remove function later
             */
            on: function(eventName, callback, name) {
                if(!this._events[eventName]) {
                    this._events[eventName] = [];
                }

                if(name) {
                    name = name + '_' + eventName;
                    this.remove(eventName, name);
                }

                this._events[eventName].push({
                    callback: callback,
                    name: name
                });
            },
            /**
             * 
             * @param {String} eventName The event to execute, this will trigger all listeners
             * @param {Array|null} args Array of arguments to provide to all callbacks
             */
            emit: function(eventName, args) {
                if(!this._events[eventName]) {
                    return false;
                }

                var actions = this._events[eventName];
                for(var i = 0; i < actions.length; i++) {
                    actions[i].callback.apply(null, args || []);
                }

                return true;
            },
            /**
             * 
             * @param {String} eventName The event to remove a callback from
             * @param {String} callbackName Name of the callback
             */
            remove: function(eventName, callbackName) {
                if(!this._events[eventName]) {
                    return false;
                }
                var actions = this._events[eventName];
                for(var i = 0; i < actions.length; i++) {
                    if(actions[i].name == callbackName) {
                        actions.splice(i, 1);
                        return true;
                    }
                }

                return false;
            },
            destroy: function(){
                tempdata.remove(this._name);
            }
        };

        tempdata.put(eventBusName, bus);
        return bus;
    }
    return tempdata.get(eventBusName);
}