# customnpcs-waifu

Some scripts for connecting chatbots into Minecraft 1.12.2 with [Noppes's Custom NPCs mod](https://beta.curseforge.com/minecraft/mc-mods/custom-npcs). Haven't tested it on newer versions of Minecraft.

![Screenshot](data/waifu.webp)

## Installation

Requires [Python](https://www.python.org/), [Forge](https://files.minecraftforge.net/net/minecraftforge/forge/index_1.12.2.html) and [Noppes's Custom NPCs mod](https://beta.curseforge.com/minecraft/mc-mods/custom-npcs)

```bash
python -m pip install --user uvicorn fastapi
```

## Usage

* `/give @p customnpcs:npcwand`
* `/give @p customnpcs:npcscripter`
* Right click with NPC wand to make waifu
* Right click the sky with scripting tool, go to Players, enable ECMAScript, go to Tab 1, put in `event_bus_with_player_events.js`
* Right click waifu with scripting tool, enable ECMAScript, go to Tab 1, put in `waifu.js`
* Run `waifu.py` FastAPI from the command line with `uvicorn waifu:app`
* If something breaks, you need to reload the scripts with `/noppes script reload`

To maintain larger projects, you can put scripts into `.minecraft/saves/{world_name}/customnpcs/scripts/ecmascript` to reuse them across characters and edit them in a proper code editor.

## Advanced

`nbt.js` can be used for modifying the NBT data of NPCs and their inventories.

## Support

If you need scripting help, the best place to ask is the Custom NPCs section of [Noppes's discord server](discord.gg/9xZuaA4)

If you have trouble with my scripts, contact me on Twitter [@robowaifudev](https://twitter.com/robowaifudev/)

## Roadmap
Creating a library for language models to interact with the game more easily.

## Contributing
Contributions are welcome.

## Authors and acknowledgment
The `event_bus.js`, `http.js` and `nbt.js` were written by other people. I don't remember where I got them. If someone knows where these scripts came from, please let me know so I can credit them.

## Project status
Not actively working on this project.
