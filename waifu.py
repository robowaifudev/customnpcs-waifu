from fastapi import FastAPI, Query

app = FastAPI()

@app.get("/chat")
def chat(message: str = Query(None)):
    return {"message": f"Hello! I got your message '{message}'"}